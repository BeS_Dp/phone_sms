﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EasyMobileSMS;


namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
      
        [TestMethod]
        public void ValidateInput_Numbers_Space_true()
        {
            string input = "223 ";
            bool expected = true;

            KeyDecoder decoder = new KeyDecoder();
            bool validation_result = decoder.ValidateInput(input);

            Assert.AreEqual(expected, validation_result);
        }

        [TestMethod]
        public void ValidateInput_numbers_letters_false()
        {
            string input = "22n3 ";
            bool expected = false;

            KeyDecoder decoder = new KeyDecoder();
            bool validation_result = decoder.ValidateInput(input);

            Assert.AreEqual(expected, validation_result);
        }

        [TestMethod]
        public void ValidateInput_numbers_with1_false()
        {
            string input = "1 223 ";
            bool expected = false;

            KeyDecoder decoder = new KeyDecoder();
            bool validation_result = decoder.ValidateInput(input);

            Assert.AreEqual(expected, validation_result);
        }

        [TestMethod]
        public void StringChecker_2233space()
        {
            string input = "2233 ";
            string[] expected = { "22", "33", null };

            KeyDecoder decoder = new KeyDecoder();
            string [] checked_string = decoder.StringChecker(input);

            CollectionAssert.AreEqual(expected, checked_string);
        }

        [TestMethod]
        public void StringChecker_23space()
        {
            string input = "23 ";
            string[] expected = { "2", "3", null };

            KeyDecoder decoder = new KeyDecoder();
            string[] checked_string = decoder.StringChecker(input);

            CollectionAssert.AreEqual(expected, checked_string);
        }

        [TestMethod]
        public void StringChecker_222333space()
        {
            string input = "222333 ";
            string[] expected = { "222", "333", null };

            KeyDecoder decoder = new KeyDecoder();
            string[] checked_string = decoder.StringChecker(input);
           
            CollectionAssert.AreEqual(expected, checked_string);
        }

        [TestMethod]
        public void StringChecker_222space2333()
        {
            string input = "222 2333 ";
            string[] expected = { "222", " ", "2", "333", null };

            KeyDecoder decoder = new KeyDecoder();
            string[] checked_string = decoder.StringChecker(input);
            CollectionAssert.AreEqual(expected, checked_string);
        }

        [TestMethod]
        public void ConvertNumberToLetter_hello()
        {
            string[] input = { "44", "33", "555", " ", "555", "666", null };
            string expected = "hello";

            KeyDecoder decoder = new KeyDecoder();
            string converted_string = decoder.ConvertNumberToLetter(input);

            Assert.AreEqual(expected, converted_string);
        }



    }
}
