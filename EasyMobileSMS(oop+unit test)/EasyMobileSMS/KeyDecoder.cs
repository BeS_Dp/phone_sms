﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EasyMobileSMS
{
    public class KeyDecoder
    {
        
        Regex regex = new Regex(@"[^0,2-9 ]"); //шаблон регулярного выражения для проверки на ввод, только цифры 


        //заполнение словаря 
        Dictionary<string, string> numbers = new Dictionary<string, string>{
                {"2", "a"},{"22", "b"},{"222", "c"},
                {"3", "d"},{"33", "e"},{"333", "f"},
                {"4", "g"},{"44", "h"},{"444", "i"},
                {"5", "j"},{"55", "k"},{"555", "l"},
                {"6", "m"},{"66", "n"},{"666", "o"},
                {"7", "p"},{"77", "q"},{"777", "r"},{"7777", "s"},
                {"8", "t"},{"88", "u"},{"888", "v"},
                {"9", "w"},{"99", "x"},{"999", "y"},{"9999", "z"},
                {"0", " "}
                };


        public string ReadSMS()
        {
            string inputstring="";
                //проверка сохранения и вывода 
                //попытка записи в массив 
            inputstring = Console.ReadLine();
            inputstring = inputstring + " "; //добавл. чтоб не выбивало ошибку индексов 
            return inputstring;
        }


        public bool ValidateInput(string inputstring)
        {
            //регулярное выражение 
            if (regex.IsMatch(inputstring))
            {
                Console.WriteLine("Only next symbols are allowed: 0,2-9");
                return false;
            }
            else return true;
        }



        public string[] StringChecker(string inputstring)
        {
            
            int j = 0;
            int counter = 0;
            string [] sbor_group = new string[inputstring.Length];

            //основная проверка 
            for (int i = 0; i < inputstring.Length - 1; i++)
            { //посл символ не с чем сравнивать, вот и < + -1 т.к. операций меньше чем чисел 
                if (inputstring[i] == inputstring[i + 1])
                { //если соседние ячейки равны 
                    sbor_group[j] = sbor_group[j] + inputstring[i];
                    counter++;
                }
                if (inputstring[i] != inputstring[i + 1])
                { //если соседние ячейки не равны 
                    if ((inputstring[i] == '0') && (i != 0) && (inputstring[i - 1] == inputstring[i + 1]))
                    {
                        counter++;
                    }
                    else
                    {
                        sbor_group[j] = sbor_group[j] + inputstring[i];
                        j++;
                    }
                }
            }

            Array.Resize(ref sbor_group, sbor_group.Length - counter);
            return sbor_group;
            //изменение размера массива, до 1 null в конце 
        }



        public string ConvertNumberToLetter(string [] sbor_group)
        {
            string itog = "";

            for (int j = 0; j < sbor_group.Length - 1; j++)
            {
                if (numbers.ContainsKey(sbor_group[j]))
                {
                    itog = itog + numbers[sbor_group[j]];
                }
            }
            return itog;
        } 


    }
}
