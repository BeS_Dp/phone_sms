﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace EasyMobileSMS
{
    class Program
    {
        static void Main(string[] args)
        {
            string input;
            string [] sbor_group;

            KeyDecoder decoder = new KeyDecoder();
            Console.Write("Enter the number of rows, which you want to decode - ");
            string line_count = Console.ReadLine();

            for (int c = 0; c < Int32.Parse(line_count); c++)
            {
                Console.Write("Enter SMS № {0} ", c + 1);
                input = decoder.ReadSMS();
                if (!decoder.ValidateInput(input)){
                break;
            }
                sbor_group = decoder.StringChecker(input);
                Console.WriteLine(decoder.ConvertNumberToLetter(sbor_group));
            }
            Console.ReadKey();
        }
    }
}